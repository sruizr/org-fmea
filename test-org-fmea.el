(require 'ert)
(require 'el-mock)

(load "/home/sruiz/Projects/org-fmea/org-fmea.el")


(defun buffer-string-no-properties ()
  (buffer-substring-no-properties 1
				 (length (buffer-string))))


(ert-deftest test-new-failure-is-inserted ()
  (let ((function-headline "* Function\n** " )
	 (expected (string-join
		    '("* Function"
		      "** Failure name"
		      "   :PROPERTIES:"
		      "   :RPN:"
		      "   :END:"
		      "*** EFFECTS"
		      "*** CAUSES"
		      "*** DETECTIONS")
		    "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert function-headline)
      (fmea-add-failure-mode "Failure name")
      (should (string-equal (buffer-string-no-properties) expected)))))


;; (ert-deftest test-new-effect-is_inserted ()
;;   (let ((expected (string-join
;; 		  '("* EFFECTS"
;; 		    "** New effect"
;; 		    "   :PROPERTIES:"
;; 		    "   :SEVERITY:"
;; 		    "   :END:")
;; 		  "\n")))
;;     (get-buffer-create "*test*")
;;     (with-current-buffer "*test*"
;;       (org-mode)
;;       (erase-buffer)
;;       (insert "* EFFECTS")
;;       (fmea-add-severity "New effect")
;;       (should (string-equal (buffer-string-no-properties) expected)))))


(ert-deftest test-plan-preventive-action ()
  (let ((expected (string-join
		   '("* Cause"
		     "** TODO Preventive action"
		     "   :PROPERTIES:"
		     "   :OCURRENCE: 5"
		     "   :END:")
		   "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert "* Cause")
      (fmea-plan-preventive-action "Preventive action" "5")
      (should (string-equal (buffer-string-no-properties) expected)))))


(ert-deftest test-go-to-failure-heading ()
  (let ((buffer-content (string-join
		   '("* Function without failure"
		     "** Subfunction without failure"
		     "* Function with failure"
		     "** Failure mode"
		     "   :PROPERTIES:"
		     "   :RPN:"
		     "   :END:"
		     "*** EFFECTS"
		     "*** CAUSES"
		     "*** DETECTIONS")
		   "\n"))
	(point-to-subfunction-without-failure 44)
	(point-to-causes 154)
	(point-to-failure-content 123)
	(point-to-failure-heading 83)
	(point-to-function 69))

    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (goto-char point-to-failure-content)
      (should (= (fmea-go-to-failure-heading) point-to-failure-heading))
      (should (= (point) point-to-failure-heading))

      (goto-char point-to-causes)
      (should (= (fmea-go-to-failure-heading) point-to-failure-heading))
      (should (= (point) point-to-failure-heading))

      (goto-char point-to-function)
      (should-not (fmea-go-to-failure-heading))
      (should (= (point) point-to-function))

      (goto-char point-to-subfunction-without-failure)
      (should-not (fmea-go-to-failure-heading))
      (should (= (point) point-to-subfunction-without-failure))
      )))


(defun prepare-test-buffer ()
  (let ((buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** EFFECTS"
			   "**** effect"
			   "     :PROPERTIES:"
			   "     :SEVERITY: 5"
			   "     :END:"
			   "*** CAUSES"
			   "**** first cause"
			   "***** root cause"
			   "      :PROPERTIES:"
			   "      :SEVERITY: 5"
			   "      :END:"
			   "*** DETECTIONS"
			   "**** DONE Detection action"
			   "     :PROPERTIES:"
			   "     :SEVERITY: 5"
			   "     :END:"
			   )
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content))))




(ert-deftest test-calculate-rpn-from-simple-structure ()
  (let ((point-to-failure-content 123))
    (prepare-test-buffer)
    (with-current-buffer "*test*"
      (goto-char point-to-failure-content)
      (mocklet ((fmea-get-severity => 5)
		(fmea-get-ocurrence => 5)
		(fmea-get-detection => 5)
		)
	(should (= (fmea-calculate-rpn) 125)))

      (mocklet ((fmea-get-severity => nil)
      		(fmea-get-ocurrence => nil)
      		(fmea-get-detection => 5))
	(should-not (fmea-calculate-rpn)))

      (mocklet ((fmea-get-severity => 5)
      		(fmea-get-ocurrence => 5)
      		(fmea-get-detection => nil))
	(should (= (fmea-calculate-rpn) 250))
	(should (= point-to-failure-content (point))))
      )))


(ert-deftest test-get-severity-return-nil-without-effects ()
  (let ((point-to-failure-content 23)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** Other content"
			   )
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (should-not (fmea-get-severity)))))


(ert-deftest test-get-severity-return-nil-with-empty-effect-list ()
  (let ((point-to-failure-content 123)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** EFFECTS"
			   )
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (should-not (fmea-get-severity)))))


(ert-deftest test-get-severity-return-nil-with-no-effects-in-list ()
  (let ((point-to-failure-content 123)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** EFFECTS"
			   "**** no effect"
			   )
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (should-not (fmea-get-severity)))))



(ert-deftest test-get-severity-return-nil-with-one-effect-in-list ()
  (let ((point-to-failure-content 123)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** EFFECTS"
			   "**** efect 1"
			   "     :PROPERTIES:"
			   "     :SEVERITY: 6"
			   "     :END:"
			   )
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (should (= 6 (fmea-get-severity))))))


(ert-deftest test-get-severity-return-max-severity ()
  (let ((point-to-failure-content 123)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** EFFECTS"
			   "**** no effect"
			   "**** effect 1"
			   "     :PROPERTIES:"
			   "     :SEVERITY: 5"
			   "     :END: "
			   "**** effect 2"
			   "     :PROPERTIES:"
			   "     :SEVERITY: 7"
			   "     :END: "

			   )
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (goto-char point-to-failure-content)
      (should (= (fmea-get-severity) 7))
      (should (= (point) point-to-failure-content)))))


(ert-deftest test-get-detection-return-10-if-no-action ()
  (let ((point-to-failure-content 52)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** DETECTIONS")
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (goto-char point-to-failure-content)
      (should (= (fmea-get-detection) 10))
      (should (= (point) point-to-failure-content))
      )))


(ert-deftest test-get-detection-return-minimal-done ()
  (let ((point-to-failure-content 52)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** DETECTIONS"
			   "**** TODO Improvement"
			   "     :PROPERTIES:"
			   "     :DETECTION: 1"
			   "     :END:"
			   "**** DONE One silly detection"
			   "     :PROPERTIES:"
			   "     :DETECTION: 5"
			   "     :END:"
			   "**** DONE Better detection"
			   "     :PROPERTIES:"
			   "     :DETECTION: 3"
			   "     :END:")
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (goto-char point-to-failure-content)
      (should (= (fmea-get-detection) 3))
      (should (= (point) point-to-failure-content))
      )))


(ert-deftest test-get-ocurrence-return-nil-if-no-root-cause ()
  (let ((point-to-failure-content 52)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** CAUSES")
			 "\n")))
    (get-buffer-create "*test*")
    (with-current-buffer "*test*"
      (org-mode)
      (erase-buffer)
      (insert buffer-content)
      (goto-char point-to-failure-content)
      (should-not (fmea-get-ocurrence))
      (should (= (point) point-to-failure-content)))))


(ert-deftest test-get-ocurrence-return-max-ocurrence-without-improvements ()
  (let ((point-to-failure-content 52)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** CAUSES"
			   "**** cause 1"
			   "***** worst root cause"
			   "      :PROPERTIES:"
			   "      :OCURRENCE: 5"
			   "      :END:"
			   "**** less important cause"
			   "     :PROPERTIES:"
			   "     :OCURRENCE: 2"
			   "     :END:")
			 "\n")))
	(get-buffer-create "*test*")
	(with-current-buffer "*test*"
	  (org-mode)
	  (erase-buffer)
	  (insert buffer-content)
	  (goto-char point-to-failure-content)
	  (should (= (fmea-get-ocurrence) 5))
	  (should (= (point) point-to-failure-content)))))


(ert-deftest test-get-ocurrence-return-max-ocurrence-with-improvements ()
  (let ((point-to-failure-content 52)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** CAUSES"
			   "**** cause 1"
			   "***** worst root cause"
			   "      :PROPERTIES:"
			   "      :OCURRENCE: 5"
			   "      :END:"
			   "****** TODO great improvement"
			   "       :PROPERTIES:"
			   "       :OCURRENCE: 2"
			   "       :END:"
			   "****** DONE improvement"
			   "       :PROPERTIES:"
			   "       :OCURRENCE: 4"
			   "       :END:"
			   "**** less important cause"
			   "     :PROPERTIES:"
			   "     :OCURRENCE: 3"
			   "     :END:")
			 "\n")))
	(get-buffer-create "*test*")
	(with-current-buffer "*test*"
	  (org-mode)
	  (erase-buffer)
	  (insert buffer-content)
	  (goto-char point-to-failure-content)
	  (should (= (fmea-get-ocurrence) 4))
	  (should (= (point) point-to-failure-content)))))


(ert-deftest test-set-rpn ()
  (let ((point-to-failure-content 123)
	(buffer-content (string-join
			 '("* Function"
			   "** Failure mode"
			   "   :PROPERTIES:"
			   "   :RPN:"
			   "   :END:"
			   "*** CAUSES"
			   "**** cause 1"
			   "***** worst root cause"
			   "      :PROPERTIES:"
			   "      :OCURRENCE: 5"
			   "      :END:"
			   "*** EFFECTS"
			   "**** Effect 1"
			   "     :PROPERTIES:"
			   "     :SEVERITY: 9"
			   "     :END:"
			   "*** DETECTIONS"
			   "**** DONE Control"
			   "     :PROPERTIES:"
			   "     :DETECTION: 3"
			   "     :END:")
			 "\n")))
	(get-buffer-create "*test*")
	(with-current-buffer "*test*"
	  (org-mode)
	  (erase-buffer)
	  (insert buffer-content)
	  (goto-char point-to-failure-content)
	  (fmea-set-RPN)
	  (should (= (fmea-get-RPN) 135))
	  )))
