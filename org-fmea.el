(require `org)

(define-minor-mode fmea-mode
  "Minor mode for developing FMEAs in org-mode"
  :lighter " fmea-org"
  :keymap  (let ((map (make-sparse-keymap)))
             (define-key map (kbd "C-c o") 'fmea-add-ocurrence)
	     (define-key map (kbd "C-c f") 'fmea-add-failure-mode)
	     (define-key map (kbd "C-c s") 'fmea-add-severity)
	     (define-key map (kbd "C-c d") 'fmea-add-detection)
	     (define-key map (kbd "C-c r") 'fmea-set-RPN)
             map))


(defvar effects-heading "EFFECTS"
  "Tag to mark on effects"
  )

(defvar causes-heading "CAUSES"
  "heading description to mark causes"
  )

(defvar detections-heading "DETECTIONS"
  "heading description to mark detections"
  )


(defun fmea-add-failure-mode (failure-name)
  (interactive "sInsert failure description: ")
  (insert failure-name)
  (org-set-property "RPN"  "")
  ;; (org-set-property "SO" "")
  (org-insert-heading-respect-content)
  (insert effects-heading)
  (org-demote)
  (org-insert-heading)
  (insert causes-heading)
  (org-insert-heading)
  (insert detections-heading))


(defun fmea-add-ocurrence (ocurrence)
  (interactive "nOcurrence: ")
  (org-set-property "OCURRENCE" (number-to-string ocurrence)))


(defun fmea-add-detection (detection)
  (interactive "nDetection: ")
  (org-set-property "DETECTION" (number-to-string detection)))


(defun fmea-add-severity (severity)
  (interactive "nSeverity: ")
  (org-set-property "SEVERITY" (number-to-string severity)))



(defun fmea-plan-preventive-action (description &optional estimated-ocurrence)
  (interactive "sDescribe preventive action: \nnEstimated ocurrence: ")
  (org-insert-todo-heading 1)
  (insert description)
  (org-demote)
  (org-set-property "OCURRENCE" estimated-ocurrence)
  )


(defun fmea-failure-mode-p ()
  (not (null (org-entry-get (point) "RPN"))))


(defun fmea-go-to-failure-heading ()
  (let ((point-to-failure))
    (save-excursion
      (outline-back-to-heading)
      (if (fmea-failure-mode-p)
	  (setq point-to-failure (point))
	(when (> (car (org-heading-components)) 1)
	  (progn
	    (outline-up-heading 1)
	    (setq point-to-failure (fmea-go-to-failure-heading))))))
    (if point-to-failure
	(goto-char point-to-failure)
      nil)))


(defun fmea-get-severity ()
  (save-excursion
    (let ((severity 0) prev-point effect-severity)
      (when (fmea--go-to-block effects-heading)
	(progn
	  (setq prev-point (point))
	  (org-next-visible-heading 1)
	  (while (/= (point) prev-point)
	    (setq effect-severity
		  (org-entry-get (point) "SEVERITY"))
	    (when (and effect-severity (not (string-equal effect-severity "")))
		(setq severity
		      (max severity (string-to-number effect-severity))))
	    (setq prev-point (point))
	    (org-forward-heading-same-level 1))
	  (when (> severity 0)
	    severity))))))


(defun fmea--go-to-block (title)
  (let (prev-position level title-point)
      (fmea-go-to-failure-heading)
      (setq prev-position (point))
      (setq level (car (org-heading-components)))
      (org-next-visible-heading 1)
      (when (> (car (org-heading-components)) level)
	  (progn
	    (while (/= (point) prev-position)
	      (when (string-equal (nth 4 (org-heading-components)) title)
	       	(setq title-point (point)))
	      (setq prev-position (point))
	      (org-forw\ard-heading-same-level 1))))
      (when title-point
	(goto-char title-point))))


(defun fmea-get-ocurrence ()
  (save-excursion
    (let ((max-ocurrence 0) prev-point ocurrence causes-level)
      (when (fmea--go-to-block causes-heading)
	(progn
	  (setq causes-level (car (org-heading-components)))
	  (setq prev-point (point))
	  (org-next-visible-heading 1)
	  (while
	      (and (/= (point) prev-point)
		   (> (car (org-heading-components)) causes-level))
	    (setq prev-point (point))
	    (if (fmea--is-root-cause)
		(setq max-ocurrence (max max-ocurrence (fmea--get-root-cause-ocurrence)))
		(org-next-visible-heading 1)))
	  (when (/= max-ocurrence 0)
	      max-ocurrence))))))

(defun fmea--is-root-cause ()
  (let ((ocurrence 0))
    (setq ocurrence
	  (org-entry-get (point) "OCURRENCE"))
    (if (and ocurrence (not (string-equal ocurrence "")))
	(> (string-to-number ocurrence) 0)
      nil)))

(defun fmea--get-root-cause-ocurrence ()
  (let (cause-level ocurrence action-ocurrence)
    (setq ocurrence (string-to-number (org-entry-get (point) "OCURRENCE")))
    (setq cause-level (car (org-heading-components)))
    (org-next-visible-heading 1)
    (while (> (car (org-heading-components)) cause-level)
      (when (string-equal (nth 2 (org-heading-components)) "DONE")
  	(setq action-ocurrence (org-entry-get (point) "OCURRENCE"))
  	(when (and action-ocurrence (not (string-equal action-ocurrence "")))
  	    (setq ocurrence (min ocurrence (string-to-number action-ocurrence)))))
      (org-next-visible-heading 1))
    ocurrence))


(defun fmea-get-detection ()
  (save-excursion
    (let ((min-detection 10) prev-point detection)
      (when (fmea--go-to-block detections-heading)
	(progn
	  (setq prev-point (point))
	  (org-next-visible-heading 1)
	  (while (/= (point) prev-point)
	    (setq detection
		  (org-entry-get (point) "DETECTION"))
	    (when
		(and detection (not (string-equal detection ""))
		     (string-equal (nth 2 (org-heading-components)) "DONE"))
	      (setq min-detection
		      (min min-detection (string-to-number detection))))
	    (setq prev-point (point))
	    (org-forward-heading-same-level 1))
	  min-detection)))))


(defun fmea-calculate-rpn ()
  (save-excursion (let (severity ocurrence detection rpn)
		    (setq severity (fmea-get-severity))
		    (setq ocurrence (fmea-get-ocurrence))
		    (setq detection (fmea-get-detection))
		    (when (and severity ocurrence)
			(setq rpn (if detection
				      (* severity ocurrence detection)
				    (* severity ocurrence 10)))))))


(defun fmea-get-RPN ()
  (interactive)
  (save-excursion
    (let (rpn)
      (fmea-go-to-failure-heading)
      (setq rpn (org-entry-get (point) "RPN"))
      (when (and rpn (not (string-equal rpn "")))
	(setq rpn (string-to-number rpn))))))


(defun fmea-set-RPN ()
  (interactive)
  (save-excursion
    (let (rpn)
    (setq rpn (fmea-calculate-rpn))
    (when rpn
      (fmea-go-to-failure-heading)
      (org-entry-put (point) "RPN" (number-to-string rpn))))))


(defun fmea-set-all-risk-priority-numbers ())
